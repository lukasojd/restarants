<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\RestaurantCourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RestaurantCourseRepository::class)
 */
class RestaurantCourse
{
    use IntIdentifier;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private string $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $name;

    /**
     * @ORM\OneToMany(targetEntity="Meal", mappedBy="restaurantCourse")
     */
    private Collection $foods;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Restaurant", inversedBy="restaurantCourses")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Restaurant $restaurant = null;

    public function __construct()
    {
        $this->foods = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<Meal>
     */
    public function getFoods(): Collection
    {
        return $this->foods;
    }

    public function addFood(Meal $food): self
    {
        if (!$this->foods->contains($food)) {
            $this->foods[] = $food;
            $food->setRestaurantCourse($this);
        }

        return $this;
    }

    public function removeFood(Meal $food): self
    {
        if ($this->foods->removeElement($food) && $food->getRestaurantCourse() === $this) {
            $food->setRestaurantCourse(null);
        }

        return $this;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }
}
