<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\MealRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MealRepository::class)
 */
class Meal
{
    use IntIdentifier;

    /**
     * @ORM\Column(type="string", length=400)
     */
    private string $name;

    /**
     * @ORM\Column(type="float")
     */
    private float $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RestaurantCourse", inversedBy="foods")
     */
    private ?RestaurantCourse $restaurantCourse = null;


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRestaurantCourse(): ?RestaurantCourse
    {
        return $this->restaurantCourse;
    }

    public function setRestaurantCourse(?RestaurantCourse $restaurantCourse): self
    {
        $this->restaurantCourse = $restaurantCourse;

        return $this;
    }
}
