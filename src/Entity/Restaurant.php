<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\RestaurantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RestaurantRepository::class)
 */
class Restaurant
{
    use IntIdentifier;

    /**
     * @ORM\Column(type="integer")
     */
    private int $restaurantId;
    /**
     * @ORM\Column(type="string", length=400)
     */
    private string $name;

    /**
     * @ORM\Column(type="text")
     */
    private string $address;

    /**
     * @ORM\Column(type="text")
     */
    private string $url;

    /**
     * @ORM\Column(type="float")
     */
    private float $gpsLat;

    /**
     * @ORM\Column(type="float")
     */
    private float $gpsLng;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RestaurantCourse", mappedBy="restaurants")
     */
    private Collection $restaurantCourses;

    public function __construct()
    {
        $this->restaurantCourses = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getGpsLat(): float
    {
        return $this->gpsLat;
    }

    public function setGpsLat(float $gpsLat): self
    {
        $this->gpsLat = $gpsLat;

        return $this;
    }

    public function getGpsLng(): float
    {
        return $this->gpsLng;
    }

    public function setGpsLng(float $gpsLng): self
    {
        $this->gpsLng = $gpsLng;

        return $this;
    }

    public function getRestaurantId(): int
    {
        return $this->restaurantId;
    }

    public function setRestaurantId(int $restaurantId): Restaurant
    {
        $this->restaurantId = $restaurantId;
        return $this;
    }

    /**
     * @return Collection|RestaurantCourse[]
     */
    public function getRestaurantCourses(): Collection
    {
        return $this->restaurantCourses;
    }

    public function addRestaurantCourse(RestaurantCourse $restaurantCourse): self
    {
        if (!$this->restaurantCourses->contains($restaurantCourse)) {
            $this->restaurantCourses[] = $restaurantCourse;
            $restaurantCourse->setRestaurant($this);
        }

        return $this;
    }

    public function removeRestaurantCourse(RestaurantCourse $restaurantCourse): self
    {
        if (
            $this->restaurantCourses->removeElement($restaurantCourse) &&
            $restaurantCourse->getRestaurant() === $this
        ) {
            $restaurantCourse->setRestaurant(null);
        }

        return $this;
    }
}
