<?php

declare(strict_types=1);

namespace App\Controller;

use App\Facade\RestaurantCourseFacade;
use App\Facade\RestaurantFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RestaurantCourseController extends AbstractController
{
    private RestaurantFacade $restaurantFacade;
    private RestaurantCourseFacade $restaurantCourseFacade;

    public function __construct(RestaurantFacade $restaurantFacade, RestaurantCourseFacade $restaurantCourseFacade)
    {
        $this->restaurantFacade = $restaurantFacade;
        $this->restaurantCourseFacade = $restaurantCourseFacade;
    }


    /**
     * @Route("/api/restaurant/courses/{restaurantId}", name="restaurant_courses")
     */
    public function index(int $restaurantId): Response
    {
        $array = [];
        $restaurant = $this->restaurantFacade->getRestaurantById($restaurantId);
        if ($restaurant !== null) {
            $array = $this->restaurantCourseFacade->getRestaurantCoursesArray($restaurant);
        }
        return $this->json($array);
    }
}
