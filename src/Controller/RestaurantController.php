<?php

declare(strict_types=1);

namespace App\Controller;

use App\Facade\RestaurantFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RestaurantController extends AbstractController
{
    private RestaurantFacade $restaurantFacade;

    public function __construct(RestaurantFacade $restaurantFacade)
    {
        $this->restaurantFacade = $restaurantFacade;
    }


    /**
     * @Route("/api/restaurants", name="restaurants")
     */
    public function index(): Response
    {
        $restaurants = $this->restaurantFacade->getRestaurantsArray();
        return $this->json($restaurants);
    }

    /**
     * @Route("/api/restaurant/{id}", name="restaurant_detail")
     */
    public function getRestaurant(int $id): Response
    {
        $restaurant = $this->restaurantFacade->getRestaurantById($id);
        $array = [];

        if ($restaurant !== null) {
            $array = $this->restaurantFacade->getRestaurantsAsArray([$restaurant]);
        }

        return $this->json(reset($array) ?: []);
    }
}
