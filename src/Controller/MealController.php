<?php

declare(strict_types=1);

namespace App\Controller;

use App\Facade\MealFacade;
use App\Facade\RestaurantCourseFacade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MealController extends AbstractController
{
    private MealFacade $mealFacade;
    private RestaurantCourseFacade $restaurantCourseFacade;

    public function __construct(MealFacade $meatFacade, RestaurantCourseFacade $restaurantCourseFacade)
    {
        $this->mealFacade = $meatFacade;
        $this->restaurantCourseFacade = $restaurantCourseFacade;
    }


    /**
     * @Route("/api/meals/{restaurantCourseId}", name="meals")
     */
    public function index(int $restaurantCourseId): Response
    {
        $restaurantCourse = $this->restaurantCourseFacade->getRestaurantCourseById($restaurantCourseId);
        $array = [];
        if ($restaurantCourse !== null) {
            $array = $this->mealFacade->getMealArray($restaurantCourse);
        }
        return $this->json($array);
    }
}
