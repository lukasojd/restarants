<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Restaurant;

class RestaurantFactory
{
    /**
     * @param mixed[] $restaurant
     */
    public function create(array $restaurant): ?Restaurant
    {
        $restaurantEntity = null;
        if (
            isset(
                $restaurant['id'],
                $restaurant['name'],
                $restaurant['address'],
                $restaurant['gps']['lat'],
                $restaurant['gps']['lng']
            )
        ) {
            $restaurantEntity = new Restaurant();
            $restaurantEntity->setName($restaurant['name']);
            $restaurantEntity->setAddress($restaurant['address']);
            $restaurantEntity->setUrl($restaurant['url']);
            $restaurantEntity->setGpsLng($restaurant['gps']['lng']);
            $restaurantEntity->setGpsLat($restaurant['gps']['lat']);
            $restaurantEntity->setRestaurantId($restaurant['id']);
        }
        return $restaurantEntity;
    }

    /**
     * @param Restaurant $restaurant
     * @return mixed[]
     */
    public function createArray(Restaurant $restaurant): array
    {
        $restaurantArray = [];
        $restaurantArray['id'] = $restaurant->getId();
        $restaurantArray['restaurantId'] = $restaurant->getRestaurantId();
        $restaurantArray['name'] = $restaurant->getName();
        $restaurantArray['address'] = $restaurant->getAddress();
        $restaurantArray['url'] = $restaurant->getUrl();
        $restaurantArray['gpsLng'] = $restaurant->getGpsLng();
        $restaurantArray['gpsLat'] = $restaurant->getGpsLat();

        return $restaurantArray;
    }
}
