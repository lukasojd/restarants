<?php

declare(strict_types=1);

namespace App\Command;

use App\Facade\RestaurantFacade;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportRestaurantDataCommand extends Command
{
    protected static $defaultName = 'app:import:restaurants';
    protected static $defaultDescription = 'Import all data from api about restaurant';

    private RestaurantFacade $restaurantFacade;

    public function __construct(string $name = null, RestaurantFacade $restaurantFacade)
    {
        parent::__construct($name);
        $this->restaurantFacade = $restaurantFacade;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->restaurantFacade->importRestaurants();

        $output->writeln("<info>Load data was successfully.</info>");

        return Command::SUCCESS;
    }
}
