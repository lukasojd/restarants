<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Restaurant;
use App\Facade\RestaurantFacade;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportRestaurantDetailCommand extends Command
{
    protected static $defaultName = 'app:import:courses';
    protected static $defaultDescription = 'Script import all course for all restaurant';
    private RestaurantFacade $restaurantFacade;

    public function __construct(string $name = null, RestaurantFacade $restaurantFacade)
    {
        parent::__construct($name);
        $this->restaurantFacade = $restaurantFacade;
    }


    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $restaurants = $this->restaurantFacade->getRestaurants();
        foreach ($restaurants as $restaurant) {
            if ($restaurant instanceof Restaurant) {
                $this->restaurantFacade->importRestaurantDetail($restaurant);
            }
        }
        return Command::SUCCESS;
    }
}
