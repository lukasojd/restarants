<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Meal;
use App\Entity\RestaurantCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Meal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Meal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Meal[]    findAll()
 * @method Meal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MealRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Meal::class);
    }

    public function createMeat(string $name, float $price, RestaurantCourse $restaurantCourse): Meal
    {
        $meal = $this->findOneBy(['name' => $name, 'restaurantCourse' => $restaurantCourse]);
        if ($meal === null) {
            $meal = new Meal();
            $meal->setName($name)
                ->setPrice($price)
                ->setRestaurantCourse($restaurantCourse);
            $em = $this->getEntityManager();
            $em->persist($meal);
            $em->flush();
        }
        return $meal;
    }
}
