<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Restaurant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Restaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Restaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Restaurant[]    findAll()
 * @method Restaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Restaurant::class);
    }

    public function getRestaurantById(int $id): ?Restaurant
    {
        return $this->findOneBy(['restaurantId' => $id]);
    }

    public function save(Restaurant $restaurant): void
    {
        $this->getEntityManager()->persist($restaurant);
        $this->getEntityManager()->flush();
    }
}
