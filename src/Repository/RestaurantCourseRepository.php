<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Restaurant;
use App\Entity\RestaurantCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RestaurantCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestaurantCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestaurantCourse[]    findAll()
 * @method RestaurantCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantCourseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RestaurantCourse::class);
    }

    public function createRestaurantCourse(string $date, string $name, Restaurant $restaurant): RestaurantCourse
    {
        $restaurantCourse = $this->findOneBy(
            ['date' => $date, 'restaurant' => $restaurant, 'name' => $name]
        );
        if ($restaurantCourse === null) {
            $restaurantCourse = new RestaurantCourse();
            $restaurantCourse->setName($name);
            $restaurantCourse->setDate($date);
            $restaurantCourse->setRestaurant($restaurant);
            $em = $this->getEntityManager();
            $em->persist($restaurantCourse);
            $em->flush();
        }

        return $restaurantCourse;
    }
}
