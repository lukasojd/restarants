<?php

declare(strict_types=1);

namespace App\Facade;

use App\Entity\Restaurant;
use App\Entity\RestaurantCourse;
use App\Repository\RestaurantCourseRepository;

class RestaurantCourseFacade
{
    private RestaurantCourseRepository $restaurantCourseRepository;

    public function __construct(RestaurantCourseRepository $restaurantCourseRepository)
    {
        $this->restaurantCourseRepository = $restaurantCourseRepository;
    }

    /**
     * @return RestaurantCourse[]
     */
    public function getRestaurantCourses(Restaurant $restaurant): array
    {
        return $this->restaurantCourseRepository->findBy(
            ['restaurant' => $restaurant],
            ['date' => 'DESC']
        );
    }


    /**
     * @return mixed[]
     */
    public function getRestaurantCoursesArray(Restaurant $restaurant): array
    {
        // todo: better method
        $courses = $this->getRestaurantCourses($restaurant);
        $coursesArray = [];

        foreach ($courses as $course) {
            $coursesArray[] = [
                'date' => $course->getDate(),
                'id' => $course->getId(),
                'name' => $course->getName(),
            ];
        }

        return $coursesArray;
    }

    public function getRestaurantCourseById(int $id): ?RestaurantCourse
    {
        return $this->restaurantCourseRepository->find($id);
    }
}
