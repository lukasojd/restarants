<?php

declare(strict_types=1);

namespace App\Facade;

use App\Entity\Meal;
use App\Entity\RestaurantCourse;
use App\Repository\MealRepository;

class MealFacade
{
    private MealRepository $meatRepository;

    public function __construct(MealRepository $mealRepository)
    {
        $this->meatRepository = $mealRepository;
    }

    /**
     * @return Meal[]
     */
    public function getMeals(RestaurantCourse $restaurantCourse): array
    {
        return $this->meatRepository->findBy(
            ['restaurantCourse' => $restaurantCourse]
        );
    }

    /**
     * @return mixed[]
     */
    public function getMealArray(RestaurantCourse $restaurantCourse): array
    {
        // todo: better method
        $meals = $this->getMeals($restaurantCourse);
        $mealsArray = [];

        foreach ($meals as $meat) {
            $mealsArray[] = [
                'id' => $meat->getId(),
                'name' => $meat->getName(),
                'price' => $meat->getPrice(),
            ];
        }

        return $mealsArray;
    }
}
