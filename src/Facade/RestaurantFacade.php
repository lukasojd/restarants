<?php

declare(strict_types=1);

namespace App\Facade;

use App\Api\RestaurantApi;
use App\Entity\Restaurant;
use App\Factory\RestaurantFactory;
use App\Repository\MealRepository;
use App\Repository\RestaurantCourseRepository;
use App\Repository\RestaurantRepository;

class RestaurantFacade
{

    private RestaurantRepository $restaurantRepository;
    private RestaurantApi $restaurantApi;
    private RestaurantFactory $restaurantFactory;
    private RestaurantCourseRepository $restaurantCourseRepository;
    private MealRepository $foodRepository;

    public function __construct(
        RestaurantRepository $restaurantRepository,
        RestaurantApi $restaurantApi,
        RestaurantFactory $restaurantFactory,
        RestaurantCourseRepository $restaurantCourseRepository,
        MealRepository $foodRepository
    ) {
        $this->restaurantRepository = $restaurantRepository;
        $this->restaurantApi = $restaurantApi;
        $this->restaurantFactory = $restaurantFactory;
        $this->restaurantCourseRepository = $restaurantCourseRepository;
        $this->foodRepository = $foodRepository;
    }

    public function importRestaurants(): void
    {
        $restaurants = $this->restaurantApi->getRestaurants();
        foreach ($restaurants as $restaurant) {
            if (isset($restaurant['id'])) {
                $restaurantEntity = $this->restaurantRepository->getRestaurantById($restaurant['id']);
                if ($restaurantEntity === null) {
                    // todo: update and remove
                    $restaurantEntity = $this->restaurantFactory->create($restaurant);
                    if ($restaurantEntity !== null) {
                        $this->restaurantRepository->save($restaurantEntity);
                    }
                }
            }
        }
    }

    public function importRestaurantDetail(Restaurant $restaurant): void
    {
        $days = $this->restaurantApi->getRestaurantDetail($restaurant->getRestaurantId());
        foreach ($days as $day) {
            if (isset($day['date']) && is_array($day['courses'])) {
                $date = $day['date'];
                foreach ($day['courses'] as $course) {
                    if (isset($course['course'])) {
                        $name = $course['course'];
                        $restaurantCourse = $this->restaurantCourseRepository->createRestaurantCourse(
                            $date,
                            $name,
                            $restaurant
                        );
                        if (is_array($course['meals'])) {
                            foreach ($course['meals'] as $meal) {
                                if (isset($meal['name'], $meal['price'])) {
                                    $this->foodRepository->createMeat(
                                        $meal['name'],
                                        $meal['price'],
                                        $restaurantCourse
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * @return Restaurant[]
     */
    public function getRestaurants(): array
    {
        return $this->restaurantRepository->findAll();
    }

    /**
     * @return mixed[]
     */
    public function getRestaurantsArray(): array
    {
        $restaurants = $this->getRestaurants();
        return $this->getRestaurantsAsArray($restaurants);
    }

    /**
     * @param Restaurant[] $restaurants
     * @return mixed[]
     */
    public function getRestaurantsAsArray(array $restaurants): array
    {
        $restaurantsArray = [];
        foreach ($restaurants as $restaurant) {
            if ($restaurant instanceof Restaurant) {
                $restaurantsArray[] = $this->restaurantFactory->createArray($restaurant);
            }
        }
        return $restaurantsArray;
    }

    public function getRestaurantById(int $id): ?Restaurant
    {
        return $this->restaurantRepository->find($id);
    }
}
