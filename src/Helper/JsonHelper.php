<?php

declare(strict_types=1);

namespace App\Helper;

use Nette\Utils\Json;
use Nette\Utils\JsonException;

class JsonHelper
{

    /**
     * @param string $json
     * @return mixed[]
     * @throws \App\Exception\JsonException
     */
    public static function decodeJson(string $json): array
    {
        try {
            $array = Json::decode($json, Json::FORCE_ARRAY);
        } catch (JsonException $jsonException) {
            throw new \App\Exception\JsonException();
        }

        return $array;
    }
}
