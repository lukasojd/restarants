<?php

declare(strict_types=1);

namespace App\Api;

use App\Exception\JsonException;
use App\Helper\JsonHelper;
use GuzzleHttp\Exception\GuzzleException;

class RestaurantApi
{
    private const RESTAURANT_API_URL = 'https://private-anon-61b3a1d941-idcrestaurant.apiary-mock.com/restaurant';
    private const RESTAURANT_DETAIL_API_URL = 'https://private-anon-61b3a1d941-idcrestaurant.apiary-mock.com/' .
    'daily-menu?restaurant_id=';

    private ApiService $apiService;

    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @return mixed[]
     * @throws JsonException
     * @throws GuzzleException
     */
    public function getRestaurants(): array
    {
        $restaurant = [];
        $restaurantResponse = $this->apiService->get(self::RESTAURANT_API_URL);
        if ($restaurantResponse->getStatusCode() === 200) {
            $json = $restaurantResponse->getBody()->getContents();
            $restaurant = JsonHelper::decodeJson($json);
        }
        return $restaurant;
    }

    /**
     * @return mixed[]
     * @throws JsonException
     * @throws GuzzleException
     */
    public function getRestaurantDetail(int $restaurantId): array
    {
        $restaurant = [];
        $restaurantResponse = $this->apiService->get(self::RESTAURANT_DETAIL_API_URL . $restaurantId);
        if ($restaurantResponse->getStatusCode() === 200) {
            $json = $restaurantResponse->getBody()->getContents();
            $restaurant = JsonHelper::decodeJson($json);
        }
        return $restaurant;
    }
}
