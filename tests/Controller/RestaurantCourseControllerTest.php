<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\RestaurantCourseController;
use App\Entity\Restaurant;
use App\Facade\RestaurantCourseFacade;
use App\Facade\RestaurantFacade;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class RestaurantCourseControllerTest extends TestCase
{
    /**
     * @var RestaurantFacade&MockObject
     */
    private $restaurantFacade;
    /**
     * @var RestaurantCourseFacade&MockObject
     */
    private $restaurantCourseFacade;
    private RestaurantCourseController $restaurantCourseController;

    protected function setUp(): void
    {
        $container = $this->createMock(ContainerInterface::class);
        $this->restaurantFacade = $this->createMock(RestaurantFacade::class);
        $this->restaurantCourseFacade = $this->createMock(RestaurantCourseFacade::class);
        $this->restaurantCourseController = new RestaurantCourseController(
            $this->restaurantFacade,
            $this->restaurantCourseFacade
        );
        $this->restaurantCourseController->setContainer($container);
    }

    public function testIndex(): void
    {
        $restaurant = new Restaurant();
        $this->restaurantFacade->expects($this->once())
            ->method('getRestaurantById')
            ->willReturn($restaurant);
        $this->restaurantCourseFacade->expects($this->once())
            ->method('getRestaurantCoursesArray')
            ->willReturn([]);
        $response = $this->restaurantCourseController->index(5);
        $this->assertSame('[]', $response->getContent());
    }
}
