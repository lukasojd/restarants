<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\MealController;
use App\Entity\Meal;
use App\Entity\RestaurantCourse;
use App\Facade\MealFacade;
use App\Facade\RestaurantCourseFacade;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class MealControllerTest extends TestCase
{

    /** @var MealFacade&MockObject */
    private $meatFacade;
    /**  @var RestaurantCourseFacade&MockObject */
    private $restaurantCourseFacade;
    private MealController $mealController;
    /** @var MockObject&ContainerInterface */
    private $container;

    public function setUp(): void
    {
        $this->container = $this->createMock(ContainerInterface::class);
        $this->meatFacade = $this->createMock(MealFacade::class);
        $this->restaurantCourseFacade = $this->createMock(RestaurantCourseFacade::class);
        $this->mealController = new MealController($this->meatFacade, $this->restaurantCourseFacade);
        $this->mealController->setContainer($this->container);
    }

    public function testIndex(): void
    {
        $restaurantCourse = new RestaurantCourse();
        $this->restaurantCourseFacade->expects($this->once())
            ->method('getRestaurantCourseById')
            ->with(5)
            ->willReturn($restaurantCourse);

        $this->meatFacade->expects($this->once())
            ->method('getMealArray')
            ->with($restaurantCourse)
            ->willReturn([]);
        $response = $this->mealController->index(5);
        $this->assertSame('[]', $response->getContent());
    }
}
