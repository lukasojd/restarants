<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Controller\RestaurantController;
use App\Entity\Restaurant;
use App\Facade\RestaurantFacade;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class RestaurantControllerTest extends TestCase
{

    /**
     * @var RestaurantFacade&MockObject
     */
    private $restaurantFacade;
    private RestaurantController $restaurantController;
    /**
     * @var mixed&ContainerInterface
     */
    private $container;

    public function setUp(): void
    {
        $this->container = $this->createMock(ContainerInterface::class);
        $this->restaurantFacade = $this->createMock(RestaurantFacade::class);
        $this->restaurantController = new RestaurantController($this->restaurantFacade);
        $this->restaurantController->setContainer($this->container);
    }

    public function testIndex(): void
    {
        $this->restaurantFacade->expects($this->once())
            ->method('getRestaurantsArray')
            ->willReturn([]);

        $response = $this->restaurantController->index();
        $this->assertSame('[]', $response->getContent());
    }

    public function testGetRestaurant(): void
    {
        $restaurant = new Restaurant();
        $this->restaurantFacade->expects($this->once())
            ->method('getRestaurantById')
            ->with(5)
            ->willReturn($restaurant);
        $this->restaurantFacade->expects($this->once())
            ->method('getRestaurantsAsArray')
            ->with([$restaurant])
            ->willReturn([]);

        $response = $this->restaurantController->getRestaurant(5);
        $this->assertSame('[]', $response->getContent());
    }
}
