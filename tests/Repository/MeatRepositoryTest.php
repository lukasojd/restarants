<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use App\Entity\Meal;
use App\Entity\RestaurantCourse;
use App\Repository\MealRepository;
use PHPUnit\Framework\TestCase;

class MeatRepositoryTest extends TestCase
{
    use RepositoryTestTrait;

    private MealRepository $foodRepository;

    public function setUp(): void
    {
        $this->setManagerRegistryMock($this);
        $this->foodRepository = new MealRepository($this->registry);
    }

    public function testShouldCreateRepository(): void
    {
        $this->assertInstanceOf(MealRepository::class, $this->foodRepository);
    }

    public function testCreateFoodExist(): void
    {
        $food = new Meal();

        $this->entityPersister->expects($this->once())
            ->method('load')
            ->willReturn($food);

        $restaurantCourse = new RestaurantCourse();

        $actualFood = $this->foodRepository->createMeat('test', 123, $restaurantCourse);
        $this->assertSame($food, $actualFood);
    }

    public function testCreateFoodNoExist(): void
    {
        $food = new Meal();

        $this->entityPersister->expects($this->once())
            ->method('load')
            ->willReturn(null);

        $restaurantCourse = new RestaurantCourse();

        $actualFood = $this->foodRepository->createMeat('test', 123, $restaurantCourse);
        $this->assertSame('test', $actualFood->getName());
        $this->assertSame(123.0, $actualFood->getPrice());
        $this->assertSame($restaurantCourse, $actualFood->getRestaurantCourse());
    }
}
