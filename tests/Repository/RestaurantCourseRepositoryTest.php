<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use App\Entity\Restaurant;
use App\Entity\RestaurantCourse;
use App\Repository\RestaurantCourseRepository;
use PHPUnit\Framework\TestCase;

class RestaurantCourseRepositoryTest extends TestCase
{
    use RepositoryTestTrait;

    private RestaurantCourseRepository $restaurantCourseRepository;

    public function setUp(): void
    {
        $this->setManagerRegistryMock($this);
        $this->restaurantCourseRepository = new RestaurantCourseRepository($this->registry);
    }

    public function testShouldCreateRepository(): void
    {
        $this->assertInstanceOf(RestaurantCourseRepository::class, $this->restaurantCourseRepository);
    }

    public function testCreateRestaurantCourseExist(): void
    {
        $restaurant = new Restaurant();
        $date = '1.1.2000';
        $name = 'Hlavní jídlo';

        $restaurantCourse = new RestaurantCourse();

        $this->entityPersister->expects($this->once())
            ->method('load')
            ->willReturn($restaurantCourse);

        $this->assertSame(
            $restaurantCourse,
            $this->restaurantCourseRepository->createRestaurantCourse($date, $name, $restaurant)
        );
    }

    public function testCreateRestaurantCourseNoExist(): void
    {
        $restaurant = new Restaurant();
        $date = '1.1.2000';
        $name = 'Hlavní jídlo';
        $this->em->expects($this->once())
            ->method('persist');
        $this->em->expects($this->once())
            ->method('flush');
        $this->entityPersister->expects($this->once())
            ->method('load')
            ->willReturn(null);
        $actual = $this->restaurantCourseRepository->createRestaurantCourse($date, $name, $restaurant);
        $this->assertSame(
            $date,
            $actual->getDate()
        );
        $this->assertSame(
            $name,
            $actual->getName()
        );
        $this->assertSame(
            $restaurant,
            $actual->getRestaurant()
        );
    }
}
