<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Persisters\Entity\EntityPersister;
use Doctrine\ORM\UnitOfWork;
use Doctrine\Persistence\ManagerRegistry;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

trait RepositoryTestTrait
{
    /** @var ManagerRegistry&MockObject */
    private $registry;
    /** @var EntityManagerInterface&MockObject */
    private $em;
    /** @var EntityPersister&MockObject */
    private $entityPersister;

    public function setManagerRegistryMock(TestCase $testCase): void
    {
        $this->entityPersister = $testCase->createMock(EntityPersister::class);

        $unitOfWork = $testCase->createMock(UnitOfWork::class);
        $unitOfWork
            ->method('getEntityPersister')
            ->willReturn($this->entityPersister);
        $em = $testCase->createMock(EntityManagerInterface::class);
        $em->method('getClassMetadata')
            ->willReturn($testCase->createMock(ClassMetadata::class));
        $em->method('getUnitOfWork')
            ->willReturn($unitOfWork);
        $managerRegistry = $testCase->createMock(ManagerRegistry::class);
        $managerRegistry->method('getManagerForClass')
            ->willReturn($em);

        $this->registry = $managerRegistry;
        $this->em = $em;
    }
}
