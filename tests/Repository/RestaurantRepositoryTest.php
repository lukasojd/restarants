<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use App\Entity\Restaurant;
use App\Repository\RestaurantRepository;
use PHPUnit\Framework\TestCase;

class RestaurantRepositoryTest extends TestCase
{
    use RepositoryTestTrait;

    private RestaurantRepository $restaurantRepository;

    public function setUp(): void
    {
        $this->setManagerRegistryMock($this);
        $this->restaurantRepository = new RestaurantRepository($this->registry);
    }

    public function testShouldCreateRepository(): void
    {
        $this->assertInstanceOf(RestaurantRepository::class, $this->restaurantRepository);
    }

    public function testGetRestaurantById(): void
    {
        $restaurant = new Restaurant();
        $this->entityPersister
            ->method('load')
            ->willReturn($restaurant);
        $this->assertSame($restaurant, $this->restaurantRepository->getRestaurantById(5));
    }

    public function testSave(): void
    {
        $restaurant = new Restaurant();

        $this->em->expects($this->once())
            ->method('persist')
            ->with($restaurant);

        $this->em->expects($this->once())
            ->method('flush');

        $this->restaurantRepository->save($restaurant);
    }
}
