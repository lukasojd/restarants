<?php

declare(strict_types=1);

namespace App\Tests\Helper;

use App\Exception\JsonException;
use App\Helper\JsonHelper;
use PHPUnit\Framework\TestCase;

class JsonHelperTest extends TestCase
{

    public function testDecodeJson(): void
    {
        $this->assertSame(['variable' => true], JsonHelper::decodeJson('{"variable": true}'));
    }

    public function testDecodeJsonException(): void
    {
        $this->expectException(JsonException::class);
        JsonHelper::decodeJson('{"');
    }
}
