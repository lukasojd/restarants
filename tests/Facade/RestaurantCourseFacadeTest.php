<?php

declare(strict_types=1);

namespace App\Tests\Facade;

use App\Entity\Restaurant;
use App\Entity\RestaurantCourse;
use App\Facade\RestaurantCourseFacade;
use App\Repository\RestaurantCourseRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RestaurantCourseFacadeTest extends TestCase
{
    private RestaurantCourseFacade $restaurantCoursesFacade;
    /**  @var RestaurantCourseRepository&MockObject */
    private $restaurantCourseRepository;

    protected function setUp(): void
    {
        $this->restaurantCourseRepository = $this->createMock(RestaurantCourseRepository::class);
        $this->restaurantCoursesFacade = new RestaurantCourseFacade($this->restaurantCourseRepository);
    }

    public function testGetRestaurantCourses(): void
    {
        $restaurantCourse = new RestaurantCourse();
        $this->restaurantCourseRepository->expects($this->once())
            ->method('findBy')
            ->willReturn([$restaurantCourse]);
        $restaurant = new Restaurant();
        $this->assertSame([$restaurantCourse], $this->restaurantCoursesFacade->getRestaurantCourses($restaurant));
    }

    public function testGetRestaurantCoursesArray(): void
    {
        $restaurantCourse = new RestaurantCourse();
        $restaurantCourse->setName('test')
            ->setDate('1.1.2011')
            ->setId(4);

        $this->restaurantCourseRepository->expects($this->once())
            ->method('findBy')
            ->willReturn([$restaurantCourse]);

        $restaurant = new Restaurant();
        $this->assertSame(
            [
                [
                    'date' => '1.1.2011',
                    'id' => 4,
                    'name' => 'test',
                ]
            ],
            $this->restaurantCoursesFacade->getRestaurantCoursesArray($restaurant)
        );
    }

    public function testGetRestaurantCourseById(): void
    {
        $restaurantCourse = new RestaurantCourse();
        $this->restaurantCourseRepository->expects($this->once())
            ->method('find')
            ->with(5)
            ->willReturn($restaurantCourse);
        $this->assertSame($restaurantCourse, $this->restaurantCoursesFacade->getRestaurantCourseById(5));
    }
}
