<?php

declare(strict_types=1);

namespace App\Tests\Facade;

use App\Entity\Meal;
use App\Entity\RestaurantCourse;
use App\Facade\MealFacade;
use App\Repository\MealRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MeatFacadeTest extends TestCase
{

    /** @var MealRepository&MockObject */
    private $meatRepository;
    private MealFacade $meatFacade;

    public function setUp(): void
    {
        $this->meatRepository = $this->createMock(MealRepository::class);
        $this->meatFacade = new MealFacade($this->meatRepository);
    }

    public function testGetMeatArray(): void
    {
        $meat = new Meal();
        $meat->setId(4)
            ->setName('test')
            ->setPrice(135.4);
        $restaurantCourse = new RestaurantCourse();
        $this->meatRepository->expects($this->once())
            ->method('findBy')
            ->willReturn([$meat]);

        $this->assertSame(
            [
                [
                    'id' => 4,
                    'name' => 'test',
                    'price' => 135.4
                ]
            ],
            $this->meatFacade->getMealArray($restaurantCourse)
        );
    }

    public function testGetMeat(): void
    {
        $meat = new Meal();
        $restaurantCourse = new RestaurantCourse();
        $this->meatRepository->expects($this->once())
            ->method('findBy')
            ->willReturn([$meat]);

        $this->assertSame([$meat], $this->meatFacade->getMeals($restaurantCourse));
    }
}
