<?php

declare(strict_types=1);

namespace App\Tests\Facade;

use App\Api\RestaurantApi;
use App\Entity\Meal;
use App\Entity\Restaurant;
use App\Entity\RestaurantCourse;
use App\Facade\RestaurantFacade;
use App\Factory\RestaurantFactory;
use App\Helper\JsonHelper;
use App\Repository\MealRepository;
use App\Repository\RestaurantCourseRepository;
use App\Repository\RestaurantRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class RestaurantFacadeTest extends TestCase
{

    /**
     * @var RestaurantApi&MockObject
     */
    private $restaurantApi;
    /**
     * @var RestaurantRepository&MockObject
     */
    private $restaurantRepository;
    private RestaurantFactory $restaurantFactory;
    private RestaurantFacade $restaurantFacade;
    /** @var RestaurantCourseRepository&MockObject */
    private $restaurantCourseRepository;
    /** @var MealRepository&MockObject */
    private $meatRepository;

    public function setUp(): void
    {
        $this->restaurantApi = $this->createMock(RestaurantApi::class);
        $this->restaurantRepository = $this->createMock(RestaurantRepository::class);
        $this->restaurantFactory = new RestaurantFactory();
        $this->restaurantCourseRepository = $this->createMock(RestaurantCourseRepository::class);
        $this->meatRepository = $this->createMock(MealRepository::class);
        $this->restaurantFacade = new RestaurantFacade(
            $this->restaurantRepository,
            $this->restaurantApi,
            $this->restaurantFactory,
            $this->restaurantCourseRepository,
            $this->meatRepository
        );
    }

    public function testImportRestaurant(): void
    {
        $this->restaurantApi->expects($this->once())
            ->method('getRestaurants')
            ->willReturn(
                [
                    [
                        'id' => 1,
                        'name' => 'Bufet Černý kohout',
                        'address' => 'Dělnická 35, Praha 7, 170 00',
                        'url' => 'https://bufetcernykohout.cz/',
                        'gps' => [
                            'lat' => 50.1033325,
                            'lng' => 14.4490758,
                        ]
                    ]
                ]
            );

        $this->restaurantRepository->expects($this->once())
            ->method('getRestaurantById')
            ->with(1)
            ->willReturn(null);

        $this->restaurantRepository->expects($this->once())
            ->method('save')
            ->with($this->isInstanceOf(Restaurant::class));

        $this->restaurantFacade->importRestaurants();
    }

    public function testGetRestaurants(): void
    {
        $this->restaurantRepository->expects($this->once())
            ->method('findAll')
            ->willReturn([]);
        $this->assertSame([], $this->restaurantFacade->getRestaurants());
    }

    public function testGetRestaurantsArray(): void
    {
        $restaurantEntity = $this->restaurantFactory
            ->create(
                [
                    'id' => 1,
                    'name' => 'Bufet Černý kohout',
                    'address' => 'Dělnická 35, Praha 7, 170 00',
                    'url' => 'https://bufetcernykohout.cz/',
                    'gps' => [
                        'lat' => 50.1033325,
                        'lng' => 14.4490758,
                    ]
                ]
            );
        $restaurantEntity->setId(5);
        $this->restaurantRepository->expects($this->once())
            ->method('findAll')
            ->willReturn([$restaurantEntity]);
        $this->assertSame(
            [
                [
                    'id' => 5,
                    'restaurantId' => 1,
                    'name' => 'Bufet Černý kohout',
                    'address' => 'Dělnická 35, Praha 7, 170 00',
                    'url' => 'https://bufetcernykohout.cz/',
                    'gpsLng' => 14.4490758,
                    'gpsLat' => 50.1033325
                ]
            ],
            $this->restaurantFacade->getRestaurantsArray()
        );
    }

    public function testImportRestaurantDetail(): void
    {
        $restaurant = new Restaurant();
        $restaurant->setRestaurantId(5);
        $this->restaurantApi->expects($this->once())
            ->method('getRestaurantDetail')
            ->with(5)
            ->willReturn(
                JsonHelper::decodeJson(
                    '[
  {
    "date": "2021-02-15",
    "courses": [
      {
        "course": "Polévky",
        "meals": [
          {
            "name": "Gulášová",
            "price": 29
          },
          {
            "name": "Hrášková s krutony",
            "price": 34
          }
        ]
      }]}]'
                )
            );
        $restaurantCourse = new RestaurantCourse();
        $food = new Meal();
        $this->restaurantCourseRepository->expects($this->once())
            ->method('createRestaurantCourse')
            ->with('2021-02-15', 'Polévky', $restaurant)
            ->willReturn($restaurantCourse);

        $this->meatRepository->expects($this->exactly(2))
            ->method('createMeat')
            ->willReturn($food);

        $this->restaurantFacade->importRestaurantDetail($restaurant);
    }

    public function testGetRestaurantById(): void
    {
        $restaurant = new Restaurant();
        $this->restaurantRepository->expects($this->once())
            ->method('find')
            ->with(5)
            ->willReturn($restaurant);
        $this->assertSame($restaurant, $this->restaurantFacade->getRestaurantById(5));
    }
}
