<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Restaurant;
use App\Entity\RestaurantCourse;
use App\Entity\RestaurantDaily;
use PHPUnit\Framework\TestCase;

class RestaurantTest extends TestCase
{
    private Restaurant $restaurant;

    protected function setUp(): void
    {
        $this->restaurant = new Restaurant();
        $this->restaurant
            ->setId(5)
            ->setName('test')
            ->setAddress('address')
            ->setUrl('url')
            ->setGpsLat(12)
            ->setGpsLng(14);
    }

    public function testCreate(): void
    {
        $this->assertSame('test', $this->restaurant->getName());
        $this->assertSame('address', $this->restaurant->getAddress());
        $this->assertSame('url', $this->restaurant->getUrl());
        $this->assertSame(12.0, $this->restaurant->getGpsLat());
        $this->assertSame(14.0, $this->restaurant->getGpsLng());
        $this->assertSame(5, $this->restaurant->getId());
    }

    public function testSetRestaurantCourse(): void
    {
        $restaurantCourse = new RestaurantCourse();
        $this->assertSame(0, $this->restaurant->getRestaurantCourses()->count());
        $this->restaurant->addRestaurantCourse($restaurantCourse);
        $this->assertSame(1, $this->restaurant->getRestaurantCourses()->count());
        $this->restaurant->removeRestaurantCourse($restaurantCourse);
        $this->assertSame(0, $this->restaurant->getRestaurantCourses()->count());
    }
}
