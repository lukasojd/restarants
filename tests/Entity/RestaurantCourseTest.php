<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Meal;
use App\Entity\Restaurant;
use App\Entity\RestaurantCourse;
use PHPUnit\Framework\TestCase;

class RestaurantCourseTest extends TestCase
{
    private RestaurantCourse $restaurantCourse;

    public function setUp(): void
    {
        $this->restaurantCourse = new RestaurantCourse();
        $this->restaurantCourse
            ->setName('test')
            ->setDate('1.1.2022');
    }

    public function testCreate(): void
    {
        $this->assertSame('test', $this->restaurantCourse->getName());
        $this->assertSame('1.1.2022', $this->restaurantCourse->getDate());
        $restaurant = new Restaurant();
        $this->restaurantCourse->setRestaurant($restaurant);
        $this->assertSame($restaurant, $this->restaurantCourse->getRestaurant());
    }

    public function testAddFood(): void
    {
        $food = new Meal();
        $this->assertSame(0, $this->restaurantCourse->getFoods()->count());
        $this->restaurantCourse->addFood($food);
        $this->assertSame(1, $this->restaurantCourse->getFoods()->count());
        $this->restaurantCourse->removeFood($food);
        $this->assertSame(0, $this->restaurantCourse->getFoods()->count());
    }
}
