<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Meal;
use PHPUnit\Framework\TestCase;

class MeatTest extends TestCase
{

    private Meal $food;

    public function setUp(): void
    {
        $this->food = new Meal();
        $this->food->setName('test');
        $this->food->setPrice(123.4);
    }

    public function testCreate(): void
    {
        $this->assertSame('test', $this->food->getName());
        $this->assertSame(123.4, $this->food->getPrice());
    }
}
