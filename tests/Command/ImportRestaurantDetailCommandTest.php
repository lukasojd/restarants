<?php

declare(strict_types=1);

namespace App\Tests\Command;

use App\Command\ImportRestaurantDetailCommand;
use App\Entity\Restaurant;
use App\Facade\RestaurantFacade;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportRestaurantDetailCommandTest extends TestCase
{

    /**
     * @var RestaurantFacade&\PHPUnit\Framework\MockObject\MockObject
     */
    private $restaurantFacade;
    private ImportRestaurantDetailCommand $importRestaurantDetailCommand;

    public function setUp(): void
    {
        $this->restaurantFacade = $this->createMock(RestaurantFacade::class);
        $this->importRestaurantDetailCommand = new ImportRestaurantDetailCommand(
            'test',
            $this->restaurantFacade
        );
    }

    public function testExecute(): void
    {
        $restaurant = new Restaurant();

        $this->restaurantFacade->expects($this->once())
            ->method('getRestaurants')
            ->willReturn([$restaurant]);

        $this->restaurantFacade->expects($this->once())
            ->method('importRestaurantDetail')
            ->with($restaurant);

        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);
        $this->importRestaurantDetailCommand->execute($input, $output);
    }
}
