<?php

declare(strict_types=1);

namespace App\Tests\Command;

use App\Command\ImportRestaurantDataCommand;
use App\Facade\RestaurantFacade;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportRestaurantDataCommandTest extends TestCase
{

    /**  @var RestaurantFacade&MockObject */
    private $restaurantFacade;
    private ImportRestaurantDataCommand $importRestaurantDataCommand;

    public function setUp(): void
    {
        $this->restaurantFacade = $this->createMock(RestaurantFacade::class);
        $this->importRestaurantDataCommand = new ImportRestaurantDataCommand('test', $this->restaurantFacade);
    }

    public function testExecute(): void
    {
        $input = $this->createMock(InputInterface::class);
        $output = $this->createMock(OutputInterface::class);

        $this->restaurantFacade->expects($this->once())
            ->method('importRestaurants');

        $this->assertSame(Command::SUCCESS, $this->importRestaurantDataCommand->execute($input, $output));
    }
}
