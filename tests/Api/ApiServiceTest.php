<?php

declare(strict_types=1);

namespace App\Tests\Api;

use App\Api\ApiService;
use GuzzleHttp\Client;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class ApiServiceTest extends TestCase
{


    /**
     * @var Client&MockObject
     */
    private $client;
    private ApiService $apiService;

    public function setUp(): void
    {
        $this->client = $this->createMock(Client::class);
        $this->apiService = new ApiService($this->client);
    }

    public function testGet(): void
    {
        $responseInterface = $this->createMock(ResponseInterface::class);
        $this->client->expects($this->once())
            ->method('get')
            ->willReturn($responseInterface);

        $this->assertSame($responseInterface, $this->apiService->get('test.cz'));
    }
}
