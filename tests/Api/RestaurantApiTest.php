<?php

declare(strict_types=1);

namespace App\Tests\Api;

use App\Api\ApiService;
use App\Api\RestaurantApi;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class RestaurantApiTest extends TestCase
{

    /** @var ApiService&MockObject */
    private $apiService;
    private RestaurantApi $restaurantApi;

    public function setUp(): void
    {
        $this->apiService = $this->createMock(ApiService::class);
        $this->restaurantApi = new RestaurantApi($this->apiService);
    }

    public function testGetRestaurants(): void
    {
        $stream = $this->createMock(StreamInterface::class);
        $stream->expects($this->once())
            ->method('getContents')
            ->willReturn(file_get_contents(__DIR__ . '/Fixtures/restaurant.json'));

        $restaurantResponse = $this->createMock(ResponseInterface::class);
        $restaurantResponse->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(200);

        $restaurantResponse->expects($this->once())
            ->method('getBody')
            ->willReturn($stream);

        $this->apiService->expects($this->once())
            ->method('get')
            ->willReturn($restaurantResponse);
        $this->assertSame(
            [
                [
                    'id' => 1,
                    'name' => 'Bufet Černý kohout',
                    'address' => 'Dělnická 35, Praha 7, 170 00',
                    'url' => 'https://bufetcernykohout.cz/',
                    'gps' => [
                        'lat' => 50.1033325,
                        'lng' => 14.4490758,
                    ]
                ]
            ],
            $this->restaurantApi->getRestaurants()
        );
    }

    public function testGetRestaurantDetail(): void
    {
        $stream = $this->createMock(StreamInterface::class);
        $stream->expects($this->once())
            ->method('getContents')
            ->willReturn('{}');

        $restaurantResponse = $this->createMock(ResponseInterface::class);
        $restaurantResponse->expects($this->once())
            ->method('getStatusCode')
            ->willReturn(200);

        $restaurantResponse->expects($this->once())
            ->method('getBody')
            ->willReturn($stream);

        $this->apiService->expects($this->once())
            ->method('get')
            ->willReturn($restaurantResponse);
        $this->assertSame(
            [
            ],
            $this->restaurantApi->getRestaurantDetail(5)
        );
    }
}
