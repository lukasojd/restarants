<?php

declare(strict_types=1);

namespace App\Tests\Factory;

use App\Factory\RestaurantFactory;
use PHPUnit\Framework\TestCase;

class RestaurantFactoryTest extends TestCase
{

    private RestaurantFactory $restaurantFactory;

    public function setUp(): void
    {
        $this->restaurantFactory = new RestaurantFactory();
    }

    public function testCreate(): void
    {
        $restaurantEntity = $this->restaurantFactory
            ->create(
                [
                    'id' => 1,
                    'name' => 'Bufet Černý kohout',
                    'address' => 'Dělnická 35, Praha 7, 170 00',
                    'url' => 'https://bufetcernykohout.cz/',
                    'gps' => [
                        'lat' => 50.1033325,
                        'lng' => 14.4490758,
                    ]
                ]
            );
        $this->assertSame(1, $restaurantEntity->getRestaurantId());
        $this->assertSame('Bufet Černý kohout', $restaurantEntity->getName());
        $this->assertSame('Dělnická 35, Praha 7, 170 00', $restaurantEntity->getAddress());
        $this->assertSame('https://bufetcernykohout.cz/', $restaurantEntity->getUrl());
        $this->assertSame(50.1033325, $restaurantEntity->getGpsLat());
        $this->assertSame(14.4490758, $restaurantEntity->getGpsLng());
    }

    public function testCreateArray(): void
    {
        $restaurantEntity = $this->restaurantFactory
            ->create(
                [
                    'id' => 1,
                    'name' => 'Bufet Černý kohout',
                    'address' => 'Dělnická 35, Praha 7, 170 00',
                    'url' => 'https://bufetcernykohout.cz/',
                    'gps' => [
                        'lat' => 50.1033325,
                        'lng' => 14.4490758,
                    ]
                ]
            );
        $restaurantEntity->setId(12345);
        $this->assertSame(
            [
                'id' => 12345,
                'restaurantId' => 1,
                'name' => 'Bufet Černý kohout',
                'address' => 'Dělnická 35, Praha 7, 170 00',
                'url' => 'https://bufetcernykohout.cz/',
                'gpsLng' => 14.4490758,
                'gpsLat' => 50.1033325,
            ],
            $this->restaurantFactory->createArray($restaurantEntity)
        );
    }
}
