#!/bin/bash

cd `dirname $0` || exit

docker-compose -f docker-compose/common.yml -f docker-compose/dev.yml  -p restaurace up --build -d
