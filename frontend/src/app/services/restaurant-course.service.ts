import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Restaurant} from "../entity/restaurant";

@Injectable({
  providedIn: 'root'
})
export class RestaurantCourseService {
  public apiURL = environment.apiEndpoint;

  constructor(protected httpClient: HttpClient) {
  }

  getCourses(restaurant: Restaurant) {
    return this.httpClient.get(`${this.apiURL}/restaurant/courses/` + restaurant.id);
  }
}
