import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {RestaurantCourse} from "../entity/restaurant-course";

@Injectable({
  providedIn: 'root'
})
export class MealService {
  public apiURL = environment.apiEndpoint;

  constructor(protected httpClient: HttpClient) {
  }

  getMeals(restaurantCourse: RestaurantCourse) {
    return this.httpClient.get(`${this.apiURL}/meals/` + restaurantCourse.id);
  }
}
