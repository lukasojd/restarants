import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  public apiURL = environment.apiEndpoint;

  constructor(protected httpClient: HttpClient) {
  }

  getRestaurants() {
    return this.httpClient.get(`${this.apiURL}/restaurants`);
  }

  getRestaurant(id: string | null) {
    return this.httpClient.get(`${this.apiURL}/restaurant/` + id);
  }

}
