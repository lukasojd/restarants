import {Component, OnInit} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-follow-pop-up',
  templateUrl: './follow-pop-up.component.html',
  styleUrls: ['./follow-pop-up.component.scss']
})
export class FollowPopUpComponent implements OnInit {
  email = null;

  constructor(private modalService: NgbModal) {
  }

  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      //todo: validate
      //todo: send email to api this.email
    });
  }


  ngOnInit(): void {
  }

}
