import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {RouterModule} from "@angular/router";
import {IconsModule, NavbarModule, WavesModule} from "angular-bootstrap-md";
import { PageTitleComponent } from './page-title/page-title.component';
import { FollowPopUpComponent } from './follow-pop-up/follow-pop-up.component';
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    PageTitleComponent,
    FollowPopUpComponent
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    PageTitleComponent,
    FollowPopUpComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    IconsModule,
    NavbarModule,
    WavesModule,
    FormsModule
  ]
})
export class CoreModule { }
