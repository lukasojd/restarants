import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DefaultComponent} from './pages/default/default.component';
import {RouterModule} from "@angular/router";
import {IconsModule} from "angular-bootstrap-md";
import { RestaurantListingComponent } from './components/restaurant-listing/restaurant-listing.component';
import {CoreModule} from "../../core/core.module";

const routing: ModuleWithProviders<any> = RouterModule.forChild([
  {path: '', component: DefaultComponent},
]);

@NgModule({
  declarations: [
    DefaultComponent,
    RestaurantListingComponent
  ],
    imports: [
        routing,
        CommonModule,
        IconsModule,
        CoreModule
    ]
})
export class HomepageModule {
}
