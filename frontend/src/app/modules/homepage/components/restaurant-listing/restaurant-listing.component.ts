import {Component, OnInit} from '@angular/core';
import {RestaurantService} from "../../../../services/restaurant.service";
import {Restaurant} from "../../../../entity/restaurant";

@Component({
  selector: 'app-restaurant-listing',
  templateUrl: './restaurant-listing.component.html',
  styleUrls: ['./restaurant-listing.component.scss']
})
export class RestaurantListingComponent implements OnInit {
  public data: Restaurant[] | undefined;

  constructor(protected restaurantService: RestaurantService) {
  }

  ngOnInit(): void {
    this.restaurantService.getRestaurants().subscribe((res: Restaurant[] | any) => {
      this.data = res;
    });
  }

}
