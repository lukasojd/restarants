import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {DefaultComponent} from "./pages/default/default.component";
import { MenuComponent } from './components/menu/menu.component';
import {NgbNavModule} from "@ng-bootstrap/ng-bootstrap";
import { MenuItemComponent } from './components/menu-item/menu-item.component';
import {CoreModule} from "../../core/core.module";
import { MealsListingComponent } from './components/meals-listing/meals-listing.component';

const routing: ModuleWithProviders<any> = RouterModule.forChild([
  {path: 'restaurace/:id', component: DefaultComponent},
]);

@NgModule({
  declarations: [
    DefaultComponent,
    MenuComponent,
    MenuItemComponent,
    MealsListingComponent
  ],
    imports: [
        routing,
        CommonModule,
        NgbNavModule,
        CoreModule
    ]
})
export class RestaurantModule {
}
