import {Component, Input, OnInit} from '@angular/core';
import {RestaurantCourse} from "../../../../entity/restaurant-course";
import {RestaurantCourseService} from "../../../../services/restaurant-course.service";
import {Restaurant} from "../../../../entity/restaurant";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input() restaurant: Restaurant | undefined;
  data: RestaurantCourse[] | undefined;
  dataGroup: Map<any, any> | undefined;

  constructor(protected restaurantCourseService: RestaurantCourseService) {
  }

  groupBy(list: RestaurantCourse[]) {
    const map = new Map();

    list.forEach((item: RestaurantCourse) => {
      let key = item.date;
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });

    return map;
  }

  ngOnInit(): void {
    if (this.restaurant !== undefined) {
      this.restaurantCourseService.getCourses(this.restaurant).subscribe((res: RestaurantCourse[] | any) => {
        this.data = res;
        this.dataGroup = this.groupBy(res);
        console.log(this.dataGroup);
      });
    }
  }

}
