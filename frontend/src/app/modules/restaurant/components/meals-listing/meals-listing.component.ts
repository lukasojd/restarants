import {Component, Input, OnInit} from '@angular/core';
import {RestaurantCourse} from "../../../../entity/restaurant-course";
import {MealService} from "../../../../services/meal.service";
import {Meal} from "../../../../entity/meal";

@Component({
  selector: 'app-meals-listing',
  templateUrl: './meals-listing.component.html',
  styleUrls: ['./meals-listing.component.scss']
})
export class MealsListingComponent implements OnInit {

  @Input() restaurantCourse: RestaurantCourse | undefined;
  data: Meal[] | undefined;

  constructor(protected mealsService: MealService) {

  }

  ngOnInit(): void {

    if (this.restaurantCourse !== undefined) {
      this.mealsService.getMeals(this.restaurantCourse)
        .subscribe((res: Meal[] | any) => {
          this.data = res;
        });
    }
  }

}
