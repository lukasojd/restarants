import {Component, Input, OnInit} from '@angular/core';
import {Meal} from "../../../../entity/meal";

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {

  @Input() meal: Meal | undefined;

  constructor() {
  }

  ngOnInit(): void {
  }

}
