import {Component, OnInit} from '@angular/core';
import {Restaurant} from "../../../../entity/restaurant";
import {RestaurantService} from "../../../../services/restaurant.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {
  restaurant: Restaurant | undefined;

  constructor(protected restaurantService: RestaurantService,
              private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    let restaurantId = this.route.snapshot.paramMap.get('id');
    this.restaurantService.getRestaurant(restaurantId)
      .subscribe((res: Restaurant | any) => {
        this.restaurant = res;
      });
  }

}
