export class RestaurantCourse {
  id: number | undefined;
  name: string | undefined;
  date: string | undefined;
}
