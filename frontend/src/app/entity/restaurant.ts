export class Restaurant {
  id: number | undefined;
  restaurantId: number | undefined;
  name: string | undefined;
  address: string | undefined;
  url: string | undefined;
  gpsLng: number | undefined;
  gpsLat: number | undefined;
}
