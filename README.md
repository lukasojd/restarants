# Testovací aplikace restaurace

## Zadání 

### Připravte jednoduchou aplikaci, která zobrazí přehled denního menu vybraných restaurací.

### První část

Jídelníčky získáte z Apiary.io na https://idcrestaurant.docs.apiary.io/
Na jedné stránce zobrazte přehled restaurací včetně základních informací. Po prokliku na další stránku zobrazte menu dané restaurace.
Vzhled není důležitý, pouze musí být stránka responzivní, aby se snadno zobrazila jak na monitoru, tak na mobilu.

### Druhá část
Připravte formulář pro zadání e-mailové adresy a seznam restaurací
Připravte službu, která sestaví uložené jídelníčky a rozešle na registrované e-maily. Služba bude spouštěna z cronu.
### Technické požadavky
Úlohu uložte do některé ze služeb dle vašich preferencí (GitHub, GitLab, Bitbucket) a zašlete odkaz.
Využít můžete libovolnou technologii, preferujeme Symfony, Nette, případně čisté PHP.
Pokud bude aplikace pokryta nějakými testy, bude to určitě bonus navíc.

## Technické řešení

### BE

- Symfony
- Doctrine
- PHP 7.4

### FE
- Angular 8

### DB 
- MySQL

## Run app
- For dev
```
composer run dev-app
```
- For production
```
composer run prod-app
```

## Run tests, phpcs, phpstan

```
composer test
```
## Fix phpcs errors

```
composer fix
```

## Dependencies

- Docker
- Docker-compose
- Bash

## Servers
- http://localhost ( local )
- https://restaurace.lukaskriz.cz ( prod )

### Crons
### Import courses
```
docker exec -it restaurace_php_1 bash
cd backend
php bin/console app:import:courses
```
### Import restaurants
```
docker exec -it restaurace_php_1 bash
cd backend
php bin/console app:import:restaurants
```

### Migration

```
# todo dump file in root project
docker exec -it restaurace_php_1 bash
cd backend
php bin/console doctrine:schema:update --force
```
## Implementation time
cca 8-10 hodin FE, cca 8 hours BE in free time

## Todo
- better map data to entity
- Server side angular ( is prepare in branch )
- write test for FE
- add logic for subscribe form
- CI/CD with deploy build javascript without manual action server
- add cron to DockerFile